package sda.fight;

public interface IFighter {
    boolean isAlive();

    // TODO: modyfikacja metody attack - musi brac pod uwage sile ciosu i rodzaj ciosu
    FighterAttackActionType attack();

    FighterDefenceActionType defend();

    // TODO: dodac parametry do ponizszej metody
    void decreaseHp(int silaCiosu);

    String getName();

    int getHp();

    int getStrength();

}
