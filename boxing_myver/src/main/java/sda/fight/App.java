package sda.fight;

public class App
{
    public static void main( String[] args ) {

        IFighter boxer1 = new Boxer("Mike Tyson", 100, 8, IFighterStyle.BALANCED);
        IFighter boxer2 = new Boxer("Andrzej Gołota", 125, 5, IFighterStyle.BALANCED);
        IFightingMatch match = new BoxingMatch(boxer1, boxer2);
        match.fight();
    }
}
