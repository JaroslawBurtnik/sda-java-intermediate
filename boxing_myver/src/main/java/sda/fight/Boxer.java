package sda.fight;

import java.util.Random;

public class Boxer implements IFighter {

    // TODO: wprowadzic sile (rozszerzajac konstruktor lub ustawic setter)
    private String name;
    private int hp;
    private IFighterStyle style;
    private int strength;

    public Boxer(String name, int hp, int strength, IFighterStyle style) {
        this.name = name;
        this.hp = hp;
        this.strength = strength;
        this.style = style;
    }

    @Override
    public boolean isAlive() {
        return hp > 0;
    }

    @Override
    public FighterAttackActionType attack() {
        Random random = new Random();

        int hitProbablility = random.nextInt(100);


        //if (hitProbablility > style.getLowPercentage()) {
        // attack = FighterAttackActionType.LOW_PUNCH;
        // } else {
        //  attack = FighterAttackActionType.HIGH_PUNCH;
        // }
        FighterAttackActionType attack = hitProbablility < style.getLowPercentage() ?
                FighterAttackActionType.LOW_PUNCH : FighterAttackActionType.HIGH_PUNCH;


        return attack;
    }

    @Override
    public FighterDefenceActionType defend() {
        Random random = new Random();
        int blockProbablility = random.nextInt(100);
        FighterDefenceActionType block = blockProbablility < style.getLowPercentage() ?
                FighterDefenceActionType.LOW_BLOCK : FighterDefenceActionType.HIGH_BLOCK;
        return block;
    }

    public IFighterStyle getStyle() {
        return style;
    }

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public void decreaseHp(int silaCiosu) {
        hp = hp - silaCiosu;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getHp() {
        return hp;
    }

}
