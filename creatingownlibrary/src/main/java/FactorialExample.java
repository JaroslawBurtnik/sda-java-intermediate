public class FactorialExample {

    public static int calculateFactorial(int number) {

        int factorial = 1;

        for (int i = 1; i <= number; i++) {
            factorial = factorial * i;
        }
        System.out.println("Factorial of " + number + " is: " + factorial);

        return factorial;
    }
}

