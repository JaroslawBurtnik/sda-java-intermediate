package sda.pres.card.issuers.impl;

import sda.pres.card.factory.IssuerRuleListFactory;
import sda.pres.card.factory.IssuerRuleSimpleFactory;
import sda.pres.card.issuers.IIssuerDetector;
import sda.pres.card.issuers.IRuleBuilder;
import sda.pres.card.issuers.IssuerRule;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class IssuerDetector implements IIssuerDetector {

    @Override
    public String detectIssuer(String cardNo, String filePath) {
        String result = "UNKNOWN";

        //TODO: ZADANIE DOMOWE: zaimplementowac klase fabryki zwracajacej obiekty typu IRuleBuilder
//        IRuleBuilder rulesBuilder = null;
//        if (filePath != null && !filePath.isEmpty()) {
//            rulesBuilder = new IssuerRuleFromFileBuilder(filePath);
//        } else {
//            rulesBuilder = new IssuerRuleBuilder();
//        }

        // TODO: utworzyc nowa fabryke lub zmodyfikowac istniejaca tak aby typem zwracanym byla List<IssuerRule>

        List<IssuerRule> issuerRules = new IssuerRuleListFactory().create(filePath);


        // algorytm przyrównuje każdą regułę do stringa cardNo przekazanego jako parametr
        // jeśli uda się dopasować regułę (program "wejdzie w ifa"), do zmiennej result
        // zostanie przypisana wartość name z obiektu reprezentującego regułę
        /**
         * for (int i = 0; i < issuerRules.size();i++) {
         *     IssuerRule issuerRule = issuerRules.get(i);
         * }
         */
        //TODO: przerobic ponizsza liste na liste map: List Map<String, String>
//        List<IssuerRule> issuerRules = rulesBuilder.buildRules();
//        for (IssuerRule issuerRule : issuerRules) {
//            if (cardNo.startsWith(String.valueOf(issuerRule.getPrefix())) && cardNo.length() == issuerRule.getLength()) {
//                result = issuerRule.getIssuerName();
//            }
//        }
//        List<Map<String, String>> issuerRulesMap =;



        return result;
    }
}
