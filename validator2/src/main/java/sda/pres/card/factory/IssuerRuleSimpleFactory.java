package sda.pres.card.factory;

import sda.pres.card.issuers.IRuleBuilder;
import sda.pres.card.issuers.impl.IssuerRuleBuilder;
import sda.pres.card.issuers.impl.IssuerRuleFromFileBuilder;

public class IssuerRuleSimpleFactory {

    public IRuleBuilder createIssuerRule(String path){

        IRuleBuilder rulesBuilder = null;
        if (path != null && !path.isEmpty()) {
            return rulesBuilder = new IssuerRuleFromFileBuilder(path);
        } else {
            return rulesBuilder = new IssuerRuleBuilder();
        }
    }

}
