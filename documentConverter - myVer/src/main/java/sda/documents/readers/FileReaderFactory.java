package sda.documents.readers;

import sda.documents.readers.CSVFileReader;
import sda.documents.readers.IFileReader;

public class FileReaderFactory {
    public IFileReader produce (String filePath) {

        IFileReader reader = null;
        // uzyc metody else/if lub uzyc split i pobrac ostatni element tablicy i sprawdzic czy jest to rozszerzenie csv.
        if (filePath.endsWith(".csv")) {
            return reader = new CSVFileReader();
        }
        if (filePath.endsWith(".json")) {
            return reader = new JSONAlternativeFileReader();
        }
        return reader;
    }
}
