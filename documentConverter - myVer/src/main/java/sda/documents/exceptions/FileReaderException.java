package sda.documents.exceptions;

public class FileReaderException extends Exception {
    public FileReaderException(String message, Throwable cause) {
        super(message, cause);
    }
}