import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class App {

    public static void main(String[] args) {

        Map<String, String> mapaDemo = new HashMap<>();
        mapaDemo.put("Cześć","Hello");
        mapaDemo.put("Do widzenia", "Goodbye");
        mapaDemo.put("Budynek", "Building");

        System.out.println(mapaDemo.get("Cześć"));
        System.out.println(mapaDemo.get("Budynek"));
//        System.out.println(mapaDemo.get("Hello")); // nie ma klucza "Hello" - otrzymamy null

        // pobieranie wartości
//        Set<String> klucze = mapaDemo.keySet();
        for (String key : mapaDemo.keySet()) {
            System.out.println(mapaDemo.get(key));
        }


        // Mapa zachowujaca kolejnosc w jakiej elementy zostaly dodane
        Map<String, String> mapaDemo2 = new LinkedHashMap<>();
        mapaDemo2.put("Cześć","Hello");
        mapaDemo2.put("Do widzenia", "Goodbye");
        mapaDemo2.put("Budynek", "Building");
    }

    public static void main2(String args) {
     Dictionary dictionary = new Dictionary();
        String translation1 = dictionary.translate("hood");
        System.out.println(dictionary.translate("hood"));

        List<String> translation2 = dictionary.translateAsList("hood");
        System.out.println(translation2);
    }
}
