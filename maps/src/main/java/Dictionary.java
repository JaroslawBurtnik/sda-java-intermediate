import java.util.*;

public class Dictionary {

        private Map<String, List<String>> dictionary = new HashMap<>();

        public Dictionary() { // konstruktor
            List<String> habitTranslations = new ArrayList<>();
            habitTranslations.add("zwyczaj");
            habitTranslations.add("habit");
            dictionary.put("habit", habitTranslations);

            String[] hoodTranslations = new String[]{"kaptur", "dach (kabrioletu)"};
            dictionary.put("hood", new ArrayList<>(Arrays.asList()));

    }

    public String translate(String wordToTranslate) {
        return dictionary.get(wordToTranslate.toLowerCase()).toString();
    }

    public List<String> translateAsList (String wordToTranslate) {
        return dictionary.get(wordToTranslate.toLowerCase());
    }
}
