package sda.cards;

import org.junit.Assert;
import org.junit.Test;
import sda.cards.interfaces.ICardValidator;

public class SDACardValidatorTest {
    @Test
    public void testValidateCardNumber() throws Exception {
        final String cardNo = "18606"; // wartosc 18606 powinna zwracac true za kazdym razem
        ICardValidator validator = new SDACardValidator();

        ValidationResult result = validator.validateCardNo(cardNo);

        Assert.assertTrue(result.isLuhnPassed());
        Assert.assertEquals(true, result.isLuhnPassed());

        Assert.assertEquals("SDA", result.getIssuer());
    }

}