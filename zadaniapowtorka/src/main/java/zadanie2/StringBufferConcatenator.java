package zadanie2;

public class StringBufferConcatenator implements StringConcatenator {
    @Override
    public String concatenate(String first, String second) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(first).append(second);

        return stringBuffer.toString();
    }
}
