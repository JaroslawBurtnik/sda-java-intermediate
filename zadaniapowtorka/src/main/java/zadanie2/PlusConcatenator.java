package zadanie2;

public class PlusConcatenator implements StringConcatenator {

    @Override
    public String concatenate(String first, String second) {
        return first + second;
    }
}
