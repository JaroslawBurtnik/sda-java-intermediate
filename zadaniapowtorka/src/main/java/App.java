
import zadanie2.StringBuilderConcatenator;
import zadanie2.StringConcatenator;

import java.util.*;

public class App {
    public static void main(String[] args) {


        List<Integer> arrayList = new ArrayList<>();
        arrayList.add(11);
        arrayList.add(5);
        arrayList.add(99);
        arrayList.add(0);
        arrayList.add(43);
        arrayList.add(99);

        Set<Integer> hashSetList = new HashSet<>();
        hashSetList.add(11);
        hashSetList.add(5);
        hashSetList.add(99);
        hashSetList.add(0);
        hashSetList.add(43);
        hashSetList.add(99);

        for (int e : arrayList) {
            System.out.print(e + "  ");
        }

        System.out.println();
        System.out.println();

        for (int e : hashSetList) {
            System.out.print(e + "  ");
        }
        System.out.println();
        System.out.println();

        ////////////////////////////////////// ZAD 2 ////////////////////////////////

        StringConcatenator example = new StringBuilderConcatenator();
        example.concatenate("wyraz1 ", "wyraz2.");

        ////////////////////////////////////// ZAD 3 ////////////////////////////////

        System.out.println();
        System.out.println();

        int a[] = new int[]{0, 71, 22, 3, 22, 52};
        float b[] = new float[]{0f, 2.22f, 3.13f, 2.22f, 5.57f};

        Integer c[] = new Integer[]{0, 71, 22, 3, 22, 52};
        Float d[] = new Float[]{0f, 2.22f, 3.13f, 2.22f, 5.57f};

        List listaA = Arrays.asList(a);
        List listaB = Arrays.asList(b);
        List listaC = Arrays.asList(c);
        List listaD = Arrays.asList(d);

        Integer dwa = 2;
        Float trzyDwadziesciaTrzy = 3.23F;

        List listaE = Arrays.asList("String1", 2, 3.23f, dwa, trzyDwadziesciaTrzy);

        System.out.println("Lista intów: " + listaA);
        System.out.println("Lista floatów: " + listaB);
        System.out.println("Lista Integerów: " + listaC);
        System.out.println("Lista Floatów: " + listaD);
        System.out.println("Lista różnych typów: " + listaE);

    }
}
